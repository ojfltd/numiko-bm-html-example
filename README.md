The purpose of this repo is to act as a demostration of my approach to building a limited version of the British Museum homepage.

This isn't supposed to be a perfect like-for-like copy, instead a time-boxed exercise to implement an accessible and responsive webpage.

[View demo](https://serene-villani-362ffd.netlify.com/)
