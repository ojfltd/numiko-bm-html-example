/**
 * Adds a `tabbing` class to the body when user is
 * navigating using keyboard
 */

const addTabbingClass = evt => {
  if (evt.which === 9) {
    document.body.classList.add('tabbing');
  }
};

const removeTabbingClass = () => document.body.classList.remove('tabbing');

document.body.addEventListener('keydown', addTabbingClass);
document.body.addEventListener('mousedown', removeTabbingClass);
