/**
 * Various functions to handle toggling the mobile menu
 */

const toggle = document.querySelector('[data-navigation-toggle]');
const menu = document.querySelector('[data-navigation]');

const mq = window.matchMedia('(max-width: 1024px)');

const toggleNavigation = () => {
  const isExpanded = toggle.getAttribute('aria-expanded');
  const isHidden = menu.getAttribute('aria-hidden');

  menu.classList.toggle('is-active');

  toggle.setAttribute(
    'aria-expanded',
    isExpanded !== 'true' ? 'true' : 'false'
  );
  menu.setAttribute('aria-hidden', isHidden !== 'true' ? 'true' : 'false');
};

const toggleNavigationVisibilityForMediaQuery = query => {
  if (query && !query.matches) {
    menu.classList.remove('is-active');

    menu.removeAttribute('aria-hidden');
    toggle.removeAttribute('aria-expanded');

    return;
  }

  toggle.setAttribute('aria-expanded', 'false');
  menu.setAttribute('aria-hidden', 'true');
};

toggle.addEventListener('click', toggleNavigation);
mq.addListener(toggleNavigationVisibilityForMediaQuery);

toggleNavigationVisibilityForMediaQuery();
