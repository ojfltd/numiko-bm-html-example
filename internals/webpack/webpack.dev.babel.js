const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = require('./webpack.base.babel')({
  mode: 'development',

  devServer: {
    contentBase: path.resolve(process.cwd(), 'dist'),
    compress: true,
    historyApiFallback: true,
    port: 3000
  },

  entry: [
    path.join(process.cwd(), 'src/js/app.js'),
    path.join(process.cwd(), 'src/css/main.scss')
  ],

  output: {
    filename: '[name].[hash].js',
    chunkFilename: '[name].[hash].js'
  },

  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
      inject: true,
      template: 'src/index.html'
    })
  ],

  module: {
    rules: []
  },

  optimization: {
    splitChunks: {
      chunks: 'all'
    }
  },

  // Emit a source map for easier debugging
  // See https://webpack.js.org/configuration/devtool/#devtool
  devtool: 'eval-source-map',

  performance: {
    hints: false
  }
});
