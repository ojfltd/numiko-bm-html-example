const path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = options => ({
  mode: options.mode,

  entry: options.entry,

  output: {
    ...options.output,
    path: path.resolve(process.cwd(), 'dist'),
    publicPath: '/'
  },

  optimization: options.optimization,

  plugins: options.plugins.concat([
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV)
    }),
    new MiniCssExtractPlugin({
      filename: '[name].css',
      chunkFilename: '[id].css'
    })
  ]),

  devtool: options.devtool,

  performance: options.performance || {},

  devServer: options.devServer,

  module: {
    rules: options.module.rules.concat([
      {
        test: /\.(gif|png|jpe?g|svg)$/i,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'img/[name].[ext]',
              esModule: false
            }
          }
        ]
      },
      {
        test: /\.scss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              hmr: process.env.NODE_ENV === 'development'
            }
          },
          'css-loader',
          'postcss-loader',
          'sass-loader'
        ]
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              [
                '@babel/env',
                {
                  targets: ['> 1%']
                }
              ]
            ],
            plugins: [
              '@babel/proposal-class-properties',
              '@babel/syntax-dynamic-import'
            ]
          }
        }
      },
      {
        test: /\.html$/,
        use: {
          loader: 'html-loader',
          options: {
            attrs: ['img:src', 'link:href'],
            root: path.resolve(__dirname, 'src')
          }
        }
      }
    ])
  },

  resolve: {
    extensions: ['.js', '.jsx', '.json', '.css', '.scss', '.svg'],
    modules: [
      path.resolve(process.cwd(), 'node_modules'),
      path.resolve(process.cwd(), 'src')
    ]
  }
});
